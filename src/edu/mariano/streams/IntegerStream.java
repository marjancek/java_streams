package edu.mariano.streams;

import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Increasing natural numbers generator
 */
public class IntegerStream implements Supplier<Integer> {
    private int val = 0;

    /**
     * Creates a sequence of consecutive, increasing integers starting from 'start'
     * 
     * @param start
     *            first value of the sequence
     */
    public IntegerStream(int start) {
        this.val = start;
    }

    @Override
    public Integer get() {
        return val++;
    }

    /**
     * Tests the IntegerStream
     * 
     * @param args
     *            are ignored
     */
    public static void main(String[] args) {
        Stream<Integer> naturalStream = Stream.generate(new IntegerStream(1));
        Stream<Integer> odds = naturalStream.filter(n -> n % 2 != 0);
        odds.skip(3).limit(30).forEach(System.out::println);
    }
}
