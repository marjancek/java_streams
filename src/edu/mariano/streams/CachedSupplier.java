package edu.mariano.streams;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * Provides access to a lazy-evaluated object instance several times, executing the evaluation only once.
 * 
 * @param <T>
 *            the type of the object to be provided
 */
public class CachedSupplier<T> implements Supplier<T> {

    private volatile T instance;
    private Supplier<T> original;

    /**
     * Creates a LazySupplier with the given lambda that computes an object of type T
     * 
     * @param supplier
     *            is the lambda, or supplier of type T
     */
    CachedSupplier(Supplier<T> supplier) {
        original = supplier;
    }

    @Override
    public T get() {
        // Lock-less test, to avoid locking after created
        if (Objects.isNull(instance)) {
            synchronized (this) {
                // We have to check again, since we might have been waiting for the
                // lock after another thread who actually created it.
                if (Objects.isNull(instance)) {
                    // Because it is volatile, all threads will see this updated value after we
                    // release the lock
                    instance = original.get();
                }
            }
        }
        return instance;
    }

    private static int value = 41;

    private static String number() {
        System.out.println("    Number computed!");
        value++;
        return Integer.toString(value);
    }

    /**
     * Makes several accesses to the given suppliers, but will use the 'callMeOnce' supplier wrapped into a
     * CachedSupplier to only call it once.
     * 
     * @param callMeMany
     *            supplier to be called several times
     * @param callMeOnce
     *            supplier to be called only once
     */
    public static void testCachedAndNot(Supplier<String> callMeMany, Supplier<String> callMeOnce) {
        CachedSupplier<String> lazy = new CachedSupplier<>(callMeOnce);

        for (int i = 0; i < 4; i++) {
            System.out.println("Try " + i + " computed " + callMeMany.get() + " // lazy: " + lazy.get());
        }
    }

    /**
     * Test that the computation only takes place once
     * 
     * @param args
     *            are ignored
     */
    public static void main(String[] args) {

        CachedSupplier<String> supplier = new CachedSupplier<>(() -> "The string " + number() + " is final");

        System.out.println("Haven't computed it yet");
        System.out.println("Calling to the Lazy String...");
        System.out.println("  This is the string: " + supplier.get());
        System.out.println("Let's call it again...");
        System.out.println("  The string is still: " + supplier.get());
        System.out.println("And again...");
        System.out.println("  The string is, you guessed it: " + supplier.get());

        testCachedAndNot(CachedSupplier::number, CachedSupplier::number);
    }
}
