package edu.mariano.streams;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Supplier based on a given supplier, which applies a given 'filter' operation (almost like a Stream)
 */
public class FilteredSupplier implements Supplier<Integer> {

    private Function<Integer, Boolean> filter;
    private Supplier<Integer> supplier;

    /**
     * Creates a supplier based on the given one, with no filter
     * 
     * @param supplier
     *            of the original sequence/values
     */
    FilteredSupplier(Supplier<Integer> supplier) {
        this(supplier, n -> true);
    }

    /**
     * Creates a supplier based on the given sequence and filter.
     * 
     * @param supplier
     *            of the original sequence/values
     * @param filter
     *            to be applied to the original sequence
     */
    FilteredSupplier(Supplier<Integer> supplier, Function<Integer, Boolean> filter) {
        this.filter = filter;
        this.supplier = supplier;
    }

    @Override
    public Integer get() {
        Integer current = supplier.get();
        // search for the first element that passes the filter.
        while (!filter.apply((current))) {
            current = supplier.get();
        }
        return current;
    }

    /**
     * Provides a sequence of prime numbers (if started with a 2)
     */
    public static class PrimeSupplier implements Supplier<Integer> {

        private Supplier<Integer> filtered = new IntegerStream(2);

        @Override
        public Integer get() {
            Integer current = filtered.get();
            filtered = new FilteredSupplier(filtered, doesntDivide(current));
            return current;
        }
    }

    /**
     * Creates a function that decides if a number is divisible by 'div'
     * 
     * @param div
     *            divisor
     * @return a function that receives an integer and decides if that integer is divisible by 'div'
     */
    public static Function<Integer, Boolean> doesntDivide(Integer div) {
        return n -> {
            // System.out.println(n+" mod "+div);
            return n % div != 0;
        };
    }

    /**
     * Tests the FilteredSupplier, and the particular PrimeSupplier
     * 
     * @param args
     *            are ignored
     */
    public static void main(String[] args) {
        final int many = 100;

        System.out.println("Some multiples of 3:");
        Stream<Integer> prod3 = Stream.generate(new FilteredSupplier(new IntegerStream(2), n -> n % 3 == 0));
        prod3.filter(n -> n % 30 != 0).skip(3).limit(10).forEach(System.out::println);

        System.out.println("\nFirst " + many + " primes");
        Stream<Integer> primes = Stream.generate(new PrimeSupplier());
        primes.limit(many).forEach(System.out::println);

    }
}
